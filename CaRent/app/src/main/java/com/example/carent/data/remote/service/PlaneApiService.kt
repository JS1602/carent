package com.example.carent.data.remote.service

import com.example.carent.data.remote.models.PlaneItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

//  falls mehr Requests notwendig sind -> "fun interface" => "interface"

fun interface PlaneApiService {

    @Headers("X-Api-Key: 4MdWKVmXMwCrL9PEfyI7NBvzcvPWrfkmLw1LB5Qx", "accept: application/json")
    @GET("v1/aircraft")
    fun getPlanes(@Query("limit") limit: Int, @Query("manufacturer") manufacturer: String): Call<List<PlaneItem>>

}