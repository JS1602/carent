package com.example.carent.screens.navigation.nav_graph

import android.util.Log
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.navArgument
import com.example.carent.screens.marketplace.bikeScreen.BikeScreen
import com.example.carent.screens.marketplace.boatScreen.BoatScreen
import com.example.carent.screens.marketplace.carScreen.CarScreen
import com.example.carent.screens.marketplace.mainScreen.MainScreen
import com.example.carent.screens.marketplace.planeScreen.PlaneScreen
import com.example.carent.screens.navigation.ArgsConstants
import com.example.carent.screens.navigation.HOME_GRAPH_ROUTE
import com.example.carent.screens.navigation.Routes

fun NavGraphBuilder.homeNavGraph(
    navController: NavHostController
){
    // ------------------ AFTER LOGIN ------------------
    navigation(
        startDestination = Routes.Main.route + "/{id}",
        arguments = (listOf(navArgument("id") {
            type = NavType.IntType
        })),
        route = "$HOME_GRAPH_ROUTE/{id}"
    ){

        // ------- HOME -------
        composable(
            route = Routes.Main.route + "/{${ArgsConstants.USER_PROFILE_KEY}}" ,
            arguments = listOf(
                navArgument(
                    name = ArgsConstants.USER_PROFILE_KEY,
                ) {
                    type = NavType.IntType
                }
            )
        ){
            val id = it.arguments?.getInt(ArgsConstants.USER_PROFILE_KEY)
            Log.d("##HomeNavGraph: Composable Routes.Main.route", "id: $id")
            MainScreen(navController = navController , transferredId = id)
        }
        // ------- Car -------
        composable(
            route = Routes.Cars.route
        ){
            CarScreen(navController = navController)
        }
        // ------- Bike -------
        composable(
            route = Routes.Bikes.route
        ){
            BikeScreen(navController = navController)
        }
        // ------- Boat -------
        composable(
            route = Routes.Boats.route
        ){
            BoatScreen(navController = navController)
        }
        // ------- Plane -------
        composable(
            route = Routes.Planes.route
        ){
            PlaneScreen(navController = navController)
        }
        // ------- Details -------
//        composable(
//            route = Routes.Details.route,
//        ) {
//            DetailScreen(navController = navController, id = 0)
//        }

//        composable(
//            route = "${Routes.Details.route}/{id}",
//            arguments = listOf(navArgument("id") { type = NavType.IntType })
//        ) { backStackEntry ->
//            val id = backStackEntry.arguments?.getInt("id") ?: 0
//            DetailScreen(navController = navController, id = id)
//        }

        // ------- Profile Nested -------
        profileNavGraph(navController = navController)
    }
}