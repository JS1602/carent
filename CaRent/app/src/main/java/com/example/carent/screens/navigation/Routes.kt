package com.example.carent.screens.navigation


const val ROOT_GRAPH_ROUTE = "root"
const val AUTH_GRAPH_ROUTE = "auth"
const val HOME_GRAPH_ROUTE = "home"
const val PROFILE_GRAPH_ROUTE = "profile"

object ArgsConstants {
    const val USER_PROFILE_KEY = "id"
}


sealed class Routes(val route: String) {

    // ------------------ Welcome ------------------
    object Welcome: Routes("welcome")

    // ------------------ Login ------------------
    object Login: Routes("login")

    // ------------------ SignUp ------------------
    object SignUp: Routes("signUp")

    // ------------------ ForgotPassword ------------------
    object ForgotPassword: Routes("forgotPassword")

    // ------------------ Main ------------------
    object Main: Routes("main/{${ArgsConstants.USER_PROFILE_KEY}}")

    // ------------------ Cars ------------------
    object Cars: Routes("cars")

    // ------------------ Bikes ------------------
    object Bikes: Routes("bikes")

    // ------------------ Boats ------------------
    object Boats: Routes("boats")

    // ------------------ Planes ------------------
    object Planes: Routes("planes")

    // ------------------ UserProfile ------------------
    object UserProfile: Routes("userProfile")

    // ------------------ Details ------------------
//    object Details: Routes("details")
}
