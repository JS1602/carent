package com.example.carent.screens.marketplace.detailScreen

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.carent.screens.util.components.CustomTopAppBar

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun DetailScreen(navController: NavController, id: Int) {

    // ------------------------------------------------------------------------------
    /* ------------------------------------------------------------------------------
    *                             !!! DISCLAIMER !!!                                *
    *   I HAVE YET TO DISCOVER HOW TO PASS THE CAR OBJECT TO THIS SCREEN            *
    *   SO FOR MY SANITY AND YOURS , PLEASE IGNORE THE FOLLOWING CODE               *
    *                                                                               *
    * -------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------
    */

//    val carItem = getItemById(id)

    Scaffold(
        topBar = { CustomTopAppBar(navController, title = "Details", showBackIcon = true) },
        content = { DetailsBody() }
    )
}


@Preview
@Composable
fun DetailPreView() {
    DetailScreen(navController = rememberNavController(), id = 0)
}

@Composable
fun DetailsBody() {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Gray)
    ) {

        // Basic details
        item {
//            car.apply {
//
//                val carImage: Painter = painterResource(R.drawable.placeholder)
//                Image(
//                    modifier = Modifier
//                        .fillMaxWidth()
//                        .height(346.dp),
//                    painter = dogImage,
//                    alignment = Alignment.CenterStart,
//                    contentDescription = "",
//                    contentScale = ContentScale.Crop
//                )
//
//                Spacer(modifier = Modifier.height(16.dp))
//                CarInfoCard(name, gender, location) TO BE MADE
        }
    }

    // Car details
//        item {
//            car.apply {
//
//                Spacer(modifier = Modifier.height(24.dp))
//                Title(title = "My Car")
//                Spacer(modifier = Modifier.height(16.dp))
//                Text(
//                    text = about,
//                    modifier = Modifier
//                        .fillMaxWidth()
//                        .padding(16.dp, 0.dp, 16.dp, 0.dp),
//                    color = colorResource(id = R.color.text),
//                    style = MaterialTheme.typography.body2,
//                    textAlign = TextAlign.Start
//                )
//            }
//        }

    // Quick info
//        item {
//            car.apply {
//
//                Spacer(modifier = Modifier.height(24.dp))
//                Title(title = "Car info")
//                Spacer(modifier = Modifier.height(16.dp))
//                Row(
//                    modifier = Modifier
//                        .fillMaxWidth()
//                        .padding(16.dp, 0.dp, 16.dp, 0.dp),
//                    horizontalArrangement = Arrangement.SpaceBetween
//                ) {
//                    InfoCard(title = "Manufacturing Year", value = car.built.toString()
//                    InfoCard(title = "Color", value = color)
//                    InfoCard(title = "Weight", value = weight.toString().plus("Kg"))
//                }
//            }
//        }

    // Owner info
//        item {
//            car.apply {
//
//                Spacer(modifier = Modifier.height(24.dp))
//                Title(title = "Owner info")
//                Spacer(modifier = Modifier.height(16.dp))
//                owner.apply {
//                    OwnerCard(name, bio, image)
//                }
//            }
//        }

    // CaRent - Rent me button
//        item {
//            Spacer(modifier = Modifier.height(36.dp))
//            Button(
//                onClick = { /* Feature missing */ },
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .height(52.dp)
//                    .padding(16.dp, 0.dp, 16.dp, 0.dp),
//                colors = ButtonDefaults.textButtonColors(
//                    backgroundColor = colorResource(id = R.color.blue),
//                    contentColor = Color.White
//                )
//            ) {
//                Text("Rent me")
//            }
//            Spacer(modifier = Modifier.height(24.dp))
//        }
}


@Composable
fun Title(title: String) {
    Text(
        text = title,
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp, 0.dp, 0.dp, 0.dp),
        color = Color.White,
        style = MaterialTheme.typography.titleSmall,
        fontWeight = FontWeight.W600,
        textAlign = TextAlign.Start
    )
}