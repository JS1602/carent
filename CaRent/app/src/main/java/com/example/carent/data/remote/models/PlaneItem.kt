package com.example.carent.data.remote.models


import com.google.gson.annotations.SerializedName

data class PlaneItem(
    @SerializedName("ceiling_ft")
    val ceilingFt: String,
    @SerializedName("cruise_speed_knots")
    val cruiseSpeedKnots: String,
    @SerializedName("empty_weight_lbs")
    val emptyWeightLbs: String,
    @SerializedName("engine_thrust_lb_ft")
    val engineThrustLbFt: String,
    @SerializedName("engine_type")
    val engineType: String,
    @SerializedName("gross_weight_lbs")
    val grossWeightLbs: String,
    @SerializedName("height_ft")
    val heightFt: String,
    @SerializedName("landing_ground_roll_ft")
    val landingGroundRollFt: String,
    @SerializedName("length_ft")
    val lengthFt: String,
    @SerializedName("manufacturer")
    val manufacturer: String,
    @SerializedName("max_speed_knots")
    val maxSpeedKnots: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("range_nautical_miles")
    val rangeNauticalMiles: String,
    @SerializedName("takeoff_ground_run_ft")
    val takeoffGroundRunFt: String,
    @SerializedName("wing_span_ft")
    val wingSpanFt: String
)