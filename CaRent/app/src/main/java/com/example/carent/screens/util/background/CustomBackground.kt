package com.example.carent.screens.util.background

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.carent.R

@Composable
fun CustomBackground() {
    Box(
        modifier = Modifier
            .fillMaxSize()
    ){
        val imageModifier = Modifier
            .fillMaxSize()
            .border(BorderStroke(1.dp, Color.Black))
        Image(
            painter = painterResource(id = R.drawable.background_canva),
            contentDescription = "Background",
            contentScale = ContentScale.Crop,
            modifier = imageModifier,
            alignment = Alignment.TopStart
        )
    }
}