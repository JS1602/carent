package com.example.carent.screens.marketplace.bikeScreen

import com.example.carent.model.GridItem

data class BikeScreenState(
    val items: List<GridItem> = mutableListOf()
)