package com.example.carent.data.room.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorites_table")
data class Favorite(
    @PrimaryKey(autoGenerate = true) val favoritesId: Int? = null,
    val userCreatorId: Int,
    val itemAddedId: Int,
)
