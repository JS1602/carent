package com.example.carent.data.room.model

import androidx.room.Embedded
import androidx.room.Relation
import com.example.carent.data.room.model.Favorite
import com.example.carent.data.room.model.User

data class UserWithFavorites(
    @Embedded val user: User,
    @Relation(
        parentColumn = "id",
        entityColumn = "userCreatorId"
    )
    val favorites: List<Favorite>
)
