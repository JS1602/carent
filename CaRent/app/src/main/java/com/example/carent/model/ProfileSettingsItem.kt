package com.example.carent.model

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector

data class ProfileSettingsItem(
    val name: String,
    val route: String,
    val icon: ImageVector,
    val tint: Color,
)
