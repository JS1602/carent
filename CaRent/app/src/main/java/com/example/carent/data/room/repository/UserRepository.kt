package com.example.carent.data.room.repository

import com.example.carent.data.room.model.UserDao
import com.example.carent.data.room.model.User
import kotlinx.coroutines.flow.Flow

class UserRepository(private val userDao: UserDao) {

    //val allUsers: Flow<List<User>> = userDao.getAllUsers()

    fun getAllUsers(): Flow<List<User>> {
        return userDao.getAllUsers()
    }

    suspend fun getUserId(email: String, password: String): Int? {
        return userDao.getUserId(email, password)
    }

    suspend fun getUserById(id: Int): User? {
        return userDao.getUserById(id)
    }

    suspend fun addUser(user: User) {
        userDao.insertUser(user)
    }

    suspend fun getUserByEmailAndPassword(email: String, password: String): User? {
        return userDao.getUserByEmailAndPassword(email, password)
    }

    suspend fun getUserByEmail(email: String): User? {
        return userDao.getUserByEmail(email)
    }

}
