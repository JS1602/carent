package com.example.carent.screens.marketplace.mainScreen

import android.app.Application
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.room.database.UserDatabase
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.launch

class MainScreenViewModel(application: Application): AndroidViewModel(application) {

    private val repository: UserRepository

    var userDetails = mutableStateOf<User?>(null)
    init {
        repository = UserRepository(UserDatabase.getDatabase(application).userDao())
    }

    // auch bei anderen umbauen ...
     fun getUserDetails(id: Int?) {
         viewModelScope.launch {
               repository.getUserById(id?:0)
             userDetails.value = repository.getUserById(id?:0)
         }
    }

//    private val repository = ImageRepository(ImageApiImplementation.createApiService())
//    private val _randomImage = MutableLiveData<RandomImage>()
//    val randomImage: LiveData<RandomImage> = _randomImage
//
//    init {
//        for(i in 0..5) {
//            Log.d("MainScreenViewModel", "init: " + repository.fetchImage(1920, 1080, "abstract").toString())
//            fetchImage()
//        }
//    }
//
//    private fun fetchImage() {
//        viewModelScope.launch{
//            try {
//                val fetchedImage = repository.fetchImage(1920, 1080, "abstract")
//                _randomImage.value = RandomImage
//            } catch (e: Exception) {
//                Log.d("MainScreenViewModel", "fetchImage: " + e.message)
//            }
//        }
//    }




}