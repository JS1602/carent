package com.example.carent.screens.authentication.loginScreen

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.carent.R
import com.example.carent.model.InputType
import com.example.carent.screens.navigation.HOME_GRAPH_ROUTE
import com.example.carent.screens.navigation.Routes
import com.example.carent.screens.util.background.LoginBackground
import com.example.carent.ui.theme.Purple80
import kotlinx.coroutines.launch


@Composable
fun LoginScreen(
    navController: NavController
) {
    val viewModel: LoginScreenViewModel = viewModel()
    Surface {
        LoginBackground()
        Login(navController, viewModel)
    }
}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Login(navController: NavController, viewModel: LoginScreenViewModel){

    val context = LocalContext.current
    val passwordFocusRequester = FocusRequester()
    val focusManager: FocusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current


    Column(
        modifier = Modifier
            .clickable {  keyboardController?.hide() }
            .padding(24.dp)
            .fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Icon(
            painter = painterResource(id = R.drawable.logo),
            null,
            tint = Color.White,
            modifier = Modifier
                .fillMaxWidth()
                .scale(3f)
        )
        TextInput(
            InputType.Username,
            keyboardActions = KeyboardActions(onNext = { passwordFocusRequester.requestFocus() }),
            onValueChanged = { email -> viewModel.setEmail(email) }
        )

        TextInput(
            InputType.Password,
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
            focusRequester = passwordFocusRequester,
            onValueChanged = { password -> viewModel.setPassword(password) }
        )

        Button(
            onClick = {
                viewModel.viewModelScope.launch {
                    val loginSuccessful = viewModel.loginUser()
                    val userId = viewModel.getUserId()
                    if (loginSuccessful) {
                        navController.navigate(route = "$HOME_GRAPH_ROUTE/$userId"){
                            popUpTo(Routes.Login.route){
                                inclusive = true
                            }
                        }
                        Log.d("###ID LoginScreen", "$userId")
                    } else {
                        Toast.makeText(
                            context,
                            "Invalid email or password",
                            Toast.LENGTH_SHORT
                        ).show()
                        viewModel.clearInputFields()
                    }
                }
            },
            colors = ButtonDefaults.buttonColors(
                containerColor =  Color.Black,
                contentColor = Color.White,
            ),
            modifier = Modifier
                .fillMaxWidth()
        )
        {
            Text(
                "Login",
                Modifier.padding(vertical = 8.dp),
                fontSize = 20.sp,
                letterSpacing = 0.15.sp
            )
        }
        Column(
            verticalArrangement = Arrangement.spacedBy(
                16.dp,
                alignment = Alignment.CenterVertically
            ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                "Don't have an account? ",
                color = Color.White,
                fontSize = 20.sp,
                letterSpacing = 0.15.sp
            )
            TextButton(onClick = { navController.navigate(Routes.SignUp.route) }) {
                Text(
                    "Sign up",
                    color = Purple80,
                    fontSize = 20.sp,
                    letterSpacing = 0.15.sp
                )
            }
            TextButton(onClick = { navController.navigate(Routes.ForgotPassword.route) }) {
                Text(
                    "Forgot password?",
                    color = Purple80,
                    fontSize = 20.sp,
                    letterSpacing = 0.15.sp
                )
            }
        }

    }
}


@Composable
fun TextInput(
    inputType: InputType,
    focusRequester: FocusRequester? = null,
    keyboardActions: KeyboardActions,
    onValueChanged: (String) -> Unit
) {
    var value: String by remember { mutableStateOf("") }

    TextField(
        value = value,
        onValueChange = {
            value = it
            onValueChanged(it)
        },
        modifier = Modifier
            .fillMaxWidth()
            .focusRequester(focusRequester ?: FocusRequester()),
        leadingIcon = { Icon(imageVector = inputType.icon, contentDescription = null) },
        label = { Text(inputType.label) },
        shape = RoundedCornerShape(8.dp),
        colors = TextFieldDefaults.colors(
            focusedContainerColor = Color.White,
            unfocusedContainerColor = Color.White,
            disabledContainerColor = Color.White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
        ),
        singleLine = true,
        keyboardOptions = inputType.keyboardOptions,
        visualTransformation = inputType.visualTransformation,
        keyboardActions = keyboardActions
    )
}

@Preview
@Composable
fun PreviewLogin() {
    LoginScreen(rememberNavController())
}
