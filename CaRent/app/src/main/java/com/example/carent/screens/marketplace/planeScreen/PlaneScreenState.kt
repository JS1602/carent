package com.example.carent.screens.marketplace.planeScreen

import com.example.carent.model.GridItem

data class PlaneScreenState(
    val items: List<GridItem> = mutableListOf()
)
