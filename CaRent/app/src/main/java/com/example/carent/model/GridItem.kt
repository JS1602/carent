package com.example.carent.model

import androidx.compose.ui.graphics.Color
import com.example.carent.model.DisplayableItems
import com.example.carent.R


// Noch umändern , damit CarGridItem bei CarView genommen wird.
data class GridItem(
    override val id: Int,
    val type: String,
    val name: String,
    val cost: Int,
    override val image: Int = R.drawable.colored_waves_removebg,
    val background: Color = Color.Yellow
) : DisplayableItems
// hier kommt noch -> : DisplayableItems , dran , damit die anderen Klassen auch GridItem erben.
