package com.example.carent.screens.authentication.welcomeScreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.room.database.UserDatabase
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class WelcomeScreenViewModel(application: Application): AndroidViewModel(application) {

    // private val readAllData: Flow<List<User>>
    private var userState: List<User> = emptyList()
    private val repository: UserRepository
    private var getUsersJob: Job? = null



    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
        getUsers()
    }

    private fun getUsers() {
        getUsersJob?.cancel()
        getUsersJob = repository.getAllUsers().onEach { users ->
            userState = users
        }.launchIn(viewModelScope)
    }


}