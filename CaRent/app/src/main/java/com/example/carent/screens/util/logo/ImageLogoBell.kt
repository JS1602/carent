package com.example.carent.screens.util.logo

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.carent.R

@Composable
fun ImageLogoBell(displayedTitle: String, displayedText: String) {
    Column() {
        Row(
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .border(2.dp, Color.White)
                .fillMaxWidth()
                .height(50.dp)
        ) {
            Image(
                modifier = Modifier
                    .offset(x = 25.dp)
                    .height(48.dp)
                    .border(2.dp, Color.Red)
                    .weight(1f),
                contentScale = ContentScale.Fit,
                painter = painterResource(id = R.drawable.logo_without_text_white),
                contentDescription = ""
            )
            Icon(
                modifier = Modifier
                    .scale(0.6f)
                    .height(48.dp)
                    .width(48.dp)
                    .border(2.dp, Color.Yellow),
                imageVector = Icons.Filled.Notifications,
                contentDescription = "",
                tint = Color.White
            )
        }

        Box(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .border(2.dp, Color.Magenta)
        ) {
            Image(
                painter = painterResource(id = R.drawable.colored_waves_removebg),
                contentDescription = "",
                contentScale = ContentScale.FillWidth,
                modifier = Modifier.border(4.dp, Color.Yellow).fillMaxWidth().height(145.dp)
            )
                Column(
                    modifier = Modifier.padding(14.dp)
                ) {
                    Text(
                        style = MaterialTheme.typography.titleLarge,
                        text = displayedTitle,
                        color = Color.White,
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(
                        style = MaterialTheme.typography.bodyMedium.copy(
                            shadow = Shadow(
                                color = Color.Black,
                                offset = Offset(x = 1f, y = 2f),
                                blurRadius = 0.2f
                            )
                        ),
                        color = Color.White,
                        text = displayedText
                    )

            }
        }
    }

}