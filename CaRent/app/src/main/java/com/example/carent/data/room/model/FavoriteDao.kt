package com.example.carent.data.room.model

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface FavoriteDao {

    @Query("SELECT * FROM favorites_table")
    fun getAllFavorites(): Flow<List<Favorite>>

    @Query("SELECT * FROM favorites_table WHERE userCreatorId = :userId")
    suspend fun getFavoritesByUserId(userId: Int): Favorite

    @Transaction
    @Query("SELECT * FROM user_table WHERE id = :userId")
    fun getUserWithFavorites(userId: Int): UserWithFavorites

    @Insert
    suspend fun insertFavorite(favorite: Favorite)

    @Update
    suspend fun updateFavorite(favorite: Favorite)

    @Delete
    suspend fun deleteFavorite(favorite: Favorite)



}