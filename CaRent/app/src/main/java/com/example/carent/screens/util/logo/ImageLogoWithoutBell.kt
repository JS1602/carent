package com.example.carent.screens.util.logo

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.carent.R

@Composable
fun ImageLogoWithoutBell(displayedTitle: String, displayedText: String) {
    Box(
        modifier = Modifier
            .padding(horizontal = 24.dp)
            .background(Brush.linearGradient(colors = listOf(
                Color(0x66FFFFFF),
                Color(0x1AFFFFFF)
            )))
    ) {
//        Image(
//            painter = painterResource(id = R.drawable.colored_waves_removebg),
//            contentDescription = "",
//            contentScale = ContentScale.FillWidth,
//            modifier = Modifier
//                .border(4.dp, Color.Yellow)
//                .fillMaxWidth()
//                .height(145.dp)
//        )
        Column(
            modifier = Modifier.padding(14.dp),
            verticalArrangement = Arrangement.Top
        ) {
            Text(

                style = MaterialTheme.typography.titleLarge,
                text = displayedTitle,
                color = Color.White,
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                style = MaterialTheme.typography.bodyMedium.copy(
                    shadow = Shadow(
                        color = Color.Black,
                        offset = Offset(x = 1f, y = 2f),
                        blurRadius = 0.2f
                    )
                ),
                color = Color.White,
                text = displayedText
            )

        }
    }
}
