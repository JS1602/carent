package com.example.carent.screens.marketplace.planeScreen

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.carent.R
import com.example.carent.screens.util.components.CustomTopAppBar
import com.example.carent.screens.util.grid.VerticalGrid

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun PlaneScreen(navController: NavController) {
    Scaffold(
        topBar = { PlaneViewTopBar(navController) },
        content = { PlaneViewContent() }
    )
}

@Composable
fun PlaneViewContent(vm: PlaneScreenViewModel = viewModel()) {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.circleloader))
    val state = vm.state.value
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
    ) {
        Column(
            modifier = Modifier.padding(top = 62.dp)
        ) {
            if (state.isNotEmpty()) {
                VerticalGrid(state)
            } else {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    LottieAnimation(composition = composition)
                }
            }
        }
    }
}

@Composable
fun PlaneViewTopBar(navController: NavController) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(58.dp)
            .background(Color(0xFF1D1E1F))
    ) {
        CustomTopAppBar(navController , title = "", showBackIcon = true )
    }
}