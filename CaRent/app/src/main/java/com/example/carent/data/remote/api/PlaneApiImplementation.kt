package com.example.carent.data.remote.api

import com.example.carent.data.remote.service.PlaneApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PlaneApiImplementation{
    companion object{

        private const val BASE_URL = "https://api.api-ninjas.com/"

        fun createApiService(): PlaneApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(PlaneApiService::class.java)
        }
    }
}