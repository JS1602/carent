package com.example.carent.screens.marketplace.boatScreen

import com.example.carent.model.GridItem

data class BoatScreenState(
    val items: List<GridItem> = mutableListOf()
)
