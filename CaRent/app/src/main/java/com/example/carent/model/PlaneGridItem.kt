package com.example.carent.model

import com.example.carent.model.DisplayableItems
import com.example.carent.R

data class PlaneGridItem(
    override val id: Int,
    val manufacturer: String,
    val model: String,
    val dailyCost: Double,
    val distanceToUser: Int,
    override val image: Int = R.drawable.placeholder,
) : DisplayableItems