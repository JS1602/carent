package com.example.carent.screens.marketplace.carScreen


import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.remote.api.CarApiImplementation
import com.example.carent.data.remote.repository.CarRepository
import com.example.carent.model.CarGridItem
import kotlinx.coroutines.launch
import java.text.DecimalFormat

class CarScreenViewModel : ViewModel() {

    private val repository = CarRepository(CarApiImplementation.createApiService())
    private val _state = mutableStateOf<List<CarGridItem>>(emptyList())
    val state: MutableState<List<CarGridItem>> = _state

    init {
        fetchCars()
    }

    private fun fetchCars() {
        viewModelScope.launch {
            val fetchedCars = repository.fetchCars(50, "audi")
            var i = 0
            fetchedCars.observeForever { carsList ->
                 _state.value = carsList.map { carItem ->
                     CarGridItem(
                         id = i++,
                         manufacturer = carItem.make,
                         model = carItem.model,
                         dailyCost = randomCost(),
                         distanceToUser = randomDistance(),
                     )
                 } ?: emptyList()
            }
        }
    }

    private fun randomCost(): Double {
        val cost = 25.00 + (Math.random() * (50.00 - 25.00))
        val decimalFormat = DecimalFormat("#.##")
        return decimalFormat.format(cost).toDouble()
    }

    private fun randomDistance(): Int {
        return (1 + (Math.random() * (50 - 1))).toInt()
    }

}