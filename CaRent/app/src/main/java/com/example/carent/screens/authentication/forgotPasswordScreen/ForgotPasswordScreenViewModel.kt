package com.example.carent.screens.authentication.forgotPasswordScreen

import android.app.Application
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import com.example.carent.data.room.database.UserDatabase
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class ForgotPasswordScreenViewModel(application: Application): AndroidViewModel(application) {

    private val readAllData: Flow<List<User>>
    private val repository: UserRepository

    private val _emailValue = mutableStateOf("")
    private val emailValue: State<String> = _emailValue

    fun setEmail(email: String) {
        _emailValue.value = email
    }

    fun emptyEmail(): Boolean {
        return emailValue.value.isEmpty()
    }

    fun clearInputFields() {
        _emailValue.value = ""
    }

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
        readAllData = repository.getAllUsers()
    }

    suspend fun requestPassword(): Boolean = withContext(Dispatchers.IO) {
        Log.d("##########", "loginUser: $emailValue.value")
        val requestedUserEmail = repository.getUserByEmail(emailValue.value)
        requestedUserEmail != null && requestedUserEmail.email == emailValue.value
    }

}