package com.example.carent.screens.authentication.signUpScreen

import android.content.Context
import android.view.Gravity
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.carent.screens.util.background.LoginBackground
import com.example.carent.data.room.model.User
import com.example.carent.screens.util.components.CustomTopAppBar
import com.example.carent.model.InputType
import com.example.carent.screens.navigation.Routes
import kotlinx.coroutines.launch

@Composable
fun SignUpScreen(
    navController: NavHostController
) {
    val viewModel: SignUpViewModel = viewModel()
    Surface{
        LoginBackground()
        Signup(navController, viewModel)
    }
}

@Preview
@Composable
fun Preview() {
    SignUpScreen(navController = rememberNavController())
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Signup(navController: NavController, viewModel: SignUpViewModel) {

    val context = LocalContext.current
    val passwordFocusRequester = FocusRequester()
    val focusManager: FocusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current
    val viewState by viewModel.viewState.collectAsState()

    // -------------------------------------- TESTING ROUTING --------------------------------------
    /*
    val backStackEntryCount = navController.backQueue.size
    val currentDestination = navController.currentBackStackEntryAsState().value?.destination?.route
    val oneBeforeCurrentDestination = navController.backQueue[backStackEntryCount - 2].destination.route
    Log.d("##### SEPERATOR ######", "---------- ####################### -----------")
    Log.d("#####currentDestination######", "Current: "+currentDestination.toString())
    for (i in 0 until backStackEntryCount) {
        println(navController.backQueue[i].destination.route)
        Log.d("## Routes ##", "$i: " + navController.backQueue[i].destination.route.toString())
    }
    Log.d("#####backStackEntryCount######", "StackCount: " + backStackEntryCount.toString())
    Log.d("#####oneBeforeCurrentDestination######", "oneBeforeCurrentDestination: "+oneBeforeCurrentDestination.toString())
    */

    CustomTopAppBar(navController, title = "Register", true)
    Box(
        modifier = Modifier.fillMaxSize().padding(top = 50.dp).clickable { keyboardController?.hide() }
    )
    Column(
        modifier = Modifier
            .padding(24.dp)
            .fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(16.dp, alignment = Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TextInputCustom(
            inputType = InputType.FirstName,
            value = viewState.valueFirstName,
            keyboardActions = KeyboardActions(onNext = { passwordFocusRequester.requestFocus() }),
            onValueChanged = { firstName ->
                viewModel.onValueChangeFirstName(firstName)
            }
        )

        TextInputCustom(
            inputType = InputType.LastName,
            value = viewState.valueLastName,
            keyboardActions = KeyboardActions(onNext = { passwordFocusRequester.requestFocus() }),
            onValueChanged = { lastName ->
                viewModel.onValueChangeLastName(lastName)
            }
        )

        TextInputCustom(
            inputType = InputType.Phone,
            value = viewState.valuePhone,
            keyboardActions = KeyboardActions(onNext = { passwordFocusRequester.requestFocus() }),
            onValueChanged = { phone ->
                viewModel.onValueChangePhone(phone)
            }
        )

        TextInputCustom(
            inputType = InputType.Email,
            value = viewState.valueEmail,
            keyboardActions = KeyboardActions(onNext = { passwordFocusRequester.requestFocus() }),
            onValueChanged = { email ->
                viewModel.onValueChangeEmail(email)
            }
        )

        TextInputCustom(
            inputType = InputType.Password,
            value = viewState.valuePassword,
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
            focusRequester = passwordFocusRequester,
            onValueChanged = { password ->
                viewModel.onValueChangePassword(password)
            }
        )

        Button(
            onClick = {
                if(!viewModel.invalidInput()){
                    val user = User(
                        firstName = viewState.valueFirstName,
                        lastName = viewState.valueLastName,
                        email = viewState.valueEmail,
                        password = viewState.valuePassword,
                        phoneNumber = viewState.valuePhone
                    )

                    viewModel.viewModelScope.launch{
                        val check = viewModel.checkIfUserExists(email = viewState.valueEmail)
                        if(check) {
                            showTopToast(context = context, "Email is already used!")
                            viewModel.clearOnButtonClick()
                        } else {
                            viewModel.addUser(user)
                            showTopToast(context = context, "User created successfully!")
                        }
                    }
                    viewModel.clearOnButtonClick()
                    navController.navigate(Routes.Login.route)
                } else {
                    showTopToast(context = context, "Please fill out all fields!")
                }
            },
            colors = ButtonDefaults.buttonColors(
                containerColor =  Color.Black,
                contentColor = Color.White,
            ),
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                "Sign up",
                Modifier.padding(vertical = 8.dp),
                fontSize = 20.sp,
                letterSpacing = 0.15.sp
            )
        }
    }
}

fun showTopToast(context: Context, message: String, duration: Int = Toast.LENGTH_SHORT) {
    val toast = Toast.makeText(context, message, duration)
    toast.setGravity(Gravity.TOP, 0, 0)
    toast.show()
}

@Composable
fun TextInputCustom(
    inputType: InputType,
    value: String,
    focusRequester: FocusRequester? = null,
    keyboardActions: KeyboardActions,
    onValueChanged: (String) -> Unit
) {
    TextField(
        value = value,
        onValueChange = { onValueChanged(it) },
        modifier = Modifier
            .fillMaxWidth()
            .focusRequester(focusRequester ?: FocusRequester()),
        leadingIcon = { Icon(imageVector = inputType.icon, contentDescription = null) },
        label = { Text(inputType.label) },
        shape = RoundedCornerShape(8.dp),
        colors = TextFieldDefaults.colors(
            focusedContainerColor = Color.White,
            unfocusedContainerColor = Color.White,
            disabledContainerColor = Color.White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
        ),
        singleLine = true,
        keyboardOptions = inputType.keyboardOptions,
        visualTransformation = inputType.visualTransformation,
        keyboardActions = keyboardActions,
    )
}