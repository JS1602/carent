package com.example.carent.screens.navigation.nav_graph

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.carent.screens.authentication.welcomeScreen.WelcomeScreen
import com.example.carent.screens.navigation.ROOT_GRAPH_ROUTE
import com.example.carent.screens.navigation.Routes

@Composable
fun SetupNavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Routes.Welcome.route,
        route = ROOT_GRAPH_ROUTE
    ) {
        composable(
            route = Routes.Welcome.route
        ){
            WelcomeScreen(navController = navController)
        }
        homeNavGraph(navController = navController)
        authNavGraph(navController = navController)
    }
}