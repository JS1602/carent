package com.example.carent.data.remote.service

import com.example.carent.data.remote.models.CarItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


//  falls mehr Requests notwendig sind -> "fun interface" => "interface"

fun interface CarApiService {
    @Headers("X-Api-Key: 4MdWKVmXMwCrL9PEfyI7NBvzcvPWrfkmLw1LB5Qx", "accept: application/json")
    @GET("v1/cars")
    fun getCars(@Query("limit") limit: Int, @Query("make") make: String): Call<List<CarItem>>
}