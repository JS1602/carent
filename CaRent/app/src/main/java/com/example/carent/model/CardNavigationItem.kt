package com.example.carent.model

data class CardNavigationItem(
    val title: String,
    val route: String,
    val icon: Int
)
