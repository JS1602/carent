package com.example.carent.screens.authentication.signUpScreen

data class SignUpScreenState(
    val valueFirstName: String = "",
    val valueLastName: String = "",
    val valuePhone: String = "",
    val valueEmail: String = "",
    val valuePassword: String = ""
)

