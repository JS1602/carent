package com.example.carent.screens.util.grid

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.carent.R
import com.example.carent.model.CarGridItem
import com.example.carent.model.DisplayableItems
import com.example.carent.model.GridItem
import com.example.carent.model.PlaneGridItem
import com.example.carent.ui.theme.CaRentTheme
import java.text.DecimalFormat

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun VerticalGrid(itemList: List<DisplayableItems>) {
    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(1),
        contentPadding = PaddingValues(16.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {

        items(itemList) { item ->
            when (item) {
                is GridItem -> ItemData(item = item)
                is CarGridItem -> ItemData(item = item)
                is PlaneGridItem -> ItemData(item = item)
                else -> error("Unsupported item type: ${item::class.java.simpleName}")
            }

        }
    }
}

@Preview
@Composable
fun Item2Prev() {
    CaRentTheme() {
        ItemData(item = CarGridItem(1, "BMW", "M3", 100.0, 100, R.drawable.placeholder))
    }
}

@Composable
fun ItemData(item: DisplayableItems) {
    when (item) {
//        is GridItem -> GenericDisplayOfContent()
        is CarGridItem -> GenericDisplayOfContent(
            id = item.id,
            manufacturer = item.manufacturer,
            model = item.model,
            price = item.dailyCost.toString(),
            distance = item.distanceToUser.toString(),
            image = item.image,
        )
        is PlaneGridItem -> GenericDisplayOfContent(
            id = item.id,
            manufacturer = item.manufacturer,
            model = item.model,
            price = item.dailyCost.toString(),
            distance = item.distanceToUser.toString(),
            image = item.image
        )

        else -> error("Unsupported item type: ${item::class.java.simpleName}")
    }
}

@Composable
fun GenericDisplayOfContent(
    id: Int,
    manufacturer: String,
    model: String,
    price: String,
    distance: String,
    image: Int
) {
    Surface(
        shape = RoundedCornerShape(16.dp),
        color = Color(0xFF305370),
        modifier = Modifier
            .height(180.dp)
            .padding(10.dp),
        shadowElevation = 10.dp
    ) {
        //                                                 Background in separate Composable
        // ------------------------------------------------------ BACKGROUND START ------------------------------------------------------
        CardBackground()
        Box(
            contentAlignment = Alignment.BottomStart,
            modifier = Modifier
                .fillMaxHeight()
                .padding(start = 320.dp, top = 10.dp, bottom = 85.dp)
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = randomReviewRating().toString(),
                    fontSize = 14.sp,
                    fontWeight = FontWeight.SemiBold,
                    style = typography.titleLarge
                )
                Spacer(modifier = Modifier.width(4.dp))
                Icon(
                    modifier = Modifier.scale(0.9f),
                    imageVector = Icons.Outlined.Star,
                    tint = Color(0xFFF6B266),
                    contentDescription = null
                )
            }
        }
        // ------------------------------------------------------ BACKGROUND END ------------------------------------------------------

        // ------------------------------------------------------ HEART BUTTON ------------------------------------------------------
        Box(
            contentAlignment = Alignment.TopStart,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 339.dp, bottom = 6.dp, top = 2.dp, end = 6.dp)
        ) {
            Surface(
                shape = RoundedCornerShape(2.dp),
                modifier = Modifier.wrapContentSize(),
                color = Color.Transparent
            ) {
                FavButton()
            }
        }
        // ------------------------------------------------------ HEART BUTTON END ------------------------------------------------------


        // ------------------------------------------------------ DETAILS BUTTON ------------------------------------------------------
        Box(
            contentAlignment = Alignment.BottomEnd,
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 6.dp, end = 6.dp)
        ) {
            OutlinedButton(
                contentPadding = PaddingValues(0.dp),
                modifier = Modifier.size(width = 100.dp, height = 24.dp),
                shape = RoundedCornerShape(8.dp),
                colors = ButtonDefaults.buttonColors(
                    contentColor = Color.Black,
                    containerColor = Color(0xFFD1D5E1)
                ),
                onClick = {
//                    navController.navigate("${Routes.Details.route}/$id")
                }
            ) {
                Text(
                    letterSpacing = 1.sp,
                    text = "Details",
                    fontSize = 12.sp,
                    fontWeight = FontWeight.SemiBold,
                    style = typography.titleLarge
                )
            }
        }
        // ------------------------------------------------------ DETAILS BUTTON END ------------------------------------------------------

        // ------------------------------------------------------ CAR IMAGE ------------------------------------------------------
        Box(
            modifier = Modifier
                .padding(10.dp)
                .height(185.dp),
        ) {
            Surface(
                shape = RoundedCornerShape(16.dp),
                modifier = Modifier.width(width = 150.dp)
            ) {
                Image(
                    painter = painterResource(id = image),
                    contentScale = ContentScale.Crop,
                    contentDescription = null
                )
            }
        }
        // ------------------------------------------------------ CAR IMAGE END ------------------------------------------------------

        // ------------------------------------------------------ CAR DETAILS ------------------------------------------------------
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .padding(start = 170.dp, top = 10.dp, bottom = 10.dp)
        ) {
            Column(
                verticalArrangement = Arrangement.spacedBy(2.dp),
            ) {
                // Manufacturer
                Surface(
                    shape = RoundedCornerShape(4.dp),
                    modifier = Modifier.wrapContentSize(),
                    color = Color(0xFFD1D5E1)
                ) {
                    Text(
                        text = manufacturer,
                        fontSize = 18.sp,
                        style = typography.titleLarge,
                        modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp)
                    )
                }

                Spacer(modifier = Modifier.height(4.dp))

                // Type
                Text(
                    text = model,
                    fontSize = 16.sp,
                    style = typography.titleLarge,
                    fontWeight = FontWeight.SemiBold
                )

                Spacer(modifier = Modifier.height(2.dp))

                // Daily Price
                Row {
                    Text(fontWeight = FontWeight.SemiBold, fontSize = 14.sp, text = "Price: ")
                    Text(fontSize = 14.sp, text = price + "€")
                    Text(fontSize = 14.sp, text = " / day")
                }
            }
            // ------------------------------------------------------ CAR DETAILS END ------------------------------------------------------

            // ------------------------------------------------------ CAR DISTANCE & ITEM ID ------------------------------------------------------
            Box(
                contentAlignment = Alignment.BottomStart,
                modifier = Modifier.fillMaxHeight()
            ) {
                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.height(45.dp)
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Icon(
                            imageVector = Icons.Filled.LocationOn,
                            contentDescription = null,
                            modifier = Modifier.size(18.dp),
                            tint = Color.Red
                        )
                        Text(
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 12.sp,
                            text = buildString {
                                append(distance)
                                append(" km")
                            }
                        )
                    }
                    Surface(
                        shape = RoundedCornerShape(2.dp),
                        modifier = Modifier.wrapContentSize(),
                        color = Color(0xFFD1D5E1)
                    ) {
                        Text(
                            text = buildString {
                                append("ID: ")
                                append(id)
//                    append(id.toString())
                            },
                            modifier = Modifier.padding(all = 2.dp),
                            fontSize = 12.sp,
                        )
                    }
                    // ------------------------------------------------------ CAR DISTANCE & ITEM ID END ------------------------------------------------------
                }
            }
        }
    }
}


// ------------------------------------------------------ COMPONENTS FOR CAR CARD ------------------------------------------------------
@Composable
fun FavButton() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .size(32.dp)
            .background(Color.Transparent, CircleShape)
    )
    {
        Icon(
            modifier = Modifier.size(24.dp),
            imageVector = Icons.Outlined.FavoriteBorder,
            contentDescription = "favorite",
            tint = Color.Black
        )
    }
}

@Composable
fun CardBackground() {
    Image(
        modifier = Modifier
            .shadow(
                200.dp,
                RoundedCornerShape(16.dp),
                ambientColor = Color.Black,
                clip = false,
                spotColor = Color.Black
            )
            .blur(3.dp)
            .fillMaxSize(),
        painter = painterResource(id = R.drawable.colored_waves_removebg),
        alignment = Alignment.CenterStart,
        contentDescription = "",
        contentScale = ContentScale.FillBounds
    )
}

// --------------------- Helper function ---------------------
// Can be deleted if rating is implemented!
private fun randomReviewRating(): Double {
    val reviewStars = 1.0 + (Math.random() * (5.0 - 1.0))
    val decimalFormat = DecimalFormat("#.#")
    return decimalFormat.format(reviewStars).toDouble()
}