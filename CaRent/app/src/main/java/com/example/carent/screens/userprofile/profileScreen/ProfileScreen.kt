package com.example.carent.screens.userprofile.profileScreen

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountBox
import androidx.compose.material.icons.filled.AppSettingsAlt
import androidx.compose.material.icons.filled.ContactPage
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.DisplaySettings
import androidx.compose.material.icons.filled.EditNotifications
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Payments
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.carent.R
import com.example.carent.model.BottomNavItem
import com.example.carent.model.ProfileSettingsItem
import com.example.carent.screens.navigation.Routes
import com.example.carent.screens.util.components.CustomTopAppBar

@Preview
@Composable
fun ProfileScreenPreview() {
    ProfileScreen(navController = rememberNavController())
}


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProfileScreen(navController: NavHostController) {
    Scaffold(
        modifier = Modifier.fillMaxHeight(),
        topBar = { TopOfProfile(navController) },
        content = { ProfileScreenContent() },
        bottomBar = { CustomMainBottomBar(navController) }
    )
}

@Composable
fun ProfileScreenContent() {
    Box(
        modifier = Modifier
            .padding(top = 100.dp)
            .background(Color.Black)
    ) {
        SettingsGrid()
    }
}

@Composable
fun TopOfProfile(navController: NavHostController) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .height(101.dp)
            .background(Color(0xFF1D1E1F))
    ) {
        CustomTopAppBar(navController, title = "", showBackIcon = true)
    }

    Box(
        modifier = Modifier.fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(R.drawable.profile_placeholder),
            contentDescription = "",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .offset(y = 55.dp)
                .clickable(onClick = { })
                .clip(CircleShape)
                .border(2.dp, Color.White, CircleShape)
                .size(95.dp)
        )
    }

}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SettingsGrid() {
    val context = LocalContext.current
    val profileSettingsItems = listOf(
        ProfileSettingsItem(
            name = "Account Settings",
            route = Routes.Main.route,
            icon = Icons.Filled.AccountBox,
            tint = Color(0xFF673AB7)
        ),
        ProfileSettingsItem(
            name = "Notifications",
            route = Routes.Main.route,
            icon = Icons.Filled.EditNotifications,
            tint = Color(0xFF9C27B0)
        ),
        ProfileSettingsItem(
            name = "General Settings",
            route = Routes.Main.route,
            icon = Icons.Filled.AppSettingsAlt,
            tint = Color(0xFF3F51B5)
        ),
        ProfileSettingsItem(
            name = "Display Settings",
            route = Routes.Main.route,
            icon = Icons.Filled.DisplaySettings,
            tint = Color(0xFF3F51B5)
        ),
        ProfileSettingsItem(
            name = "Payments",
            route = Routes.Main.route,
            icon = Icons.Filled.Payments,
            tint = Color(0xFF4CAF50)
        ),
        ProfileSettingsItem(
            name = "About us",
            route = Routes.Main.route,
            icon = Icons.Filled.Info,
            tint = Color(0xFFFF9800)
        ),
        ProfileSettingsItem(
            name = "Contact us",
            route = Routes.Main.route,
            icon = Icons.Filled.ContactPage,
            tint = Color(0xFFFF9800)
        ),
        ProfileSettingsItem(
            name = "Delete Account",
            route = Routes.Main.route,
            icon = Icons.Filled.DeleteForever,
            tint = Color(0xFFEC3023)
        ),
    )


    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(1),
        contentPadding = PaddingValues(16.dp),
        horizontalArrangement = Arrangement.spacedBy(22.dp),
        modifier = Modifier
            .fillMaxHeight()
            .padding(top = 70.dp)
    ) {
        items(profileSettingsItems) { item ->
            Row(
                modifier = Modifier
                    .clickable(onClick = {
                        Toast.makeText(context, "Feature will be added soon!", Toast.LENGTH_SHORT).show()
                    })
                    .clip(RoundedCornerShape(35.dp))
                    .padding(8.dp)
                    .fillMaxWidth()
                    .height(50.dp)
                    .background(Color(0xFF1D1E1F))
                    .border(2.dp, Color.Black, RoundedCornerShape(4.dp))
                    .clickable(onClick = { })
                    .padding(8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(24.dp)
            ) {
                Icon(
                    modifier = Modifier.padding(start = 8.dp),
                    imageVector = item.icon,
                    contentDescription = "",
                    tint = item.tint
                )
                Text(
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 16.sp,
                    text = item.name,
                )
            }
        }

    }
}

@Composable
fun CustomMainBottomBar(navController: NavController) {
    val bottomNavItems = listOf(
        BottomNavItem(
            name = "Home",
            route = Routes.Main.route,
            icon = Icons.Outlined.Home
        ),
        BottomNavItem(
            name = "Favorites",
            route = Routes.Main.route,
            icon = Icons.Outlined.Favorite
        ),
        BottomNavItem(
            name = "Settings",
            route = Routes.Main.route,
            icon = Icons.Outlined.Settings
        )
    )

    NavigationBar(
        containerColor = Color.Transparent,
        modifier = Modifier
            .height(55.dp)
            .background(Color(0xFF1D1E1F))
    ) {
        bottomNavItems.forEach { item ->
            NavigationBarItem(
                selected = false,
                onClick = { navController.navigate(item.route) },
                label = {
                    Text(
                        modifier = Modifier.offset(0.dp, 10.dp),
                        text = item.name,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White
                    )
                },
                icon = {
                    Icon(
                        modifier = Modifier.offset(0.dp, (-10).dp),
                        imageVector = item.icon,
                        contentDescription = "${item.name} Icon",
                        tint = Color.White
                    )
                }
            )
        }
    }
}