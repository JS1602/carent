package com.example.carent.screens.authentication.signUpScreen

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.room.database.UserDatabase
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    private val readAllData: Flow<List<User>>
    private val repository: UserRepository

    private val _viewState = MutableStateFlow(SignUpScreenState())

    val viewState: StateFlow<SignUpScreenState> = _viewState


    fun clearOnButtonClick() {
        _viewState.value = _viewState.value.copy(
            valueFirstName = "",
            valueLastName = "",
            valuePhone = "",
            valueEmail = "",
            valuePassword = "",
        )
    }

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
        readAllData = repository.getAllUsers()
    }

    fun onValueChangeFirstName(newValue: String) {
        _viewState.value = _viewState.value.copy(valueFirstName = newValue)
    }

    fun onValueChangeLastName(newValue: String) {
        _viewState.value = _viewState.value.copy(valueLastName = newValue)
    }

    fun onValueChangePhone(newValue: String) {
        _viewState.value = _viewState.value.copy(valuePhone = newValue)
    }

    fun onValueChangeEmail(newValue: String) {
        _viewState.value = _viewState.value.copy(valueEmail = newValue)
    }

    fun onValueChangePassword(newValue: String) {
        _viewState.value = _viewState.value.copy(valuePassword = newValue)
    }

    suspend fun checkIfUserExists(email: String): Boolean {
        return repository.getUserByEmail(email) != null
    }

    fun invalidInput(): Boolean {
        return _viewState.value.valueFirstName.isEmpty() ||
                _viewState.value.valueLastName.isEmpty() ||
                _viewState.value.valuePhone.isEmpty() ||
                _viewState.value.valueEmail.isEmpty() ||
                _viewState.value.valuePassword.isEmpty()
    }

    fun addUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addUser(user)
            Log.d("SignUpViewModel", "User inserted: $user")
        }
    }

}
