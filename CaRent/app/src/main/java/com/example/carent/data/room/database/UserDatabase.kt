package com.example.carent.data.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.carent.data.room.model.Favorite
import com.example.carent.data.room.model.FavoriteDao
import com.example.carent.data.room.model.User
import com.example.carent.data.room.model.UserDao

@Database(entities = [User::class, Favorite::class], version = 3, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun favoritesDao(): FavoriteDao
    companion object {
        @Volatile
        private var INSTANCE: UserDatabase? = null

       fun getDatabase (context: Context): UserDatabase {
           val tempInstance = INSTANCE
           if(tempInstance != null) {
               return tempInstance
           }
           synchronized(this) {
               val instance = Room.databaseBuilder(
                   context.applicationContext,
                   UserDatabase::class.java,
                     "user_database"
               )
                   .fallbackToDestructiveMigration()
                   .build()
               INSTANCE = instance
               return instance
           }
       }

    }


}