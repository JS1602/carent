package com.example.carent.data.remote.api

import com.example.carent.data.remote.service.CarApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CarApiImplementation{
    companion object{

        private const val BASE_URL = "https://api.api-ninjas.com/"

        fun createApiService(): CarApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(CarApiService::class.java)
        }
    }
}