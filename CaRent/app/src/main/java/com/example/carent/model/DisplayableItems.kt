package com.example.carent.model

// Zeugs das bei allen gleich ist, kommt hier rein.
interface DisplayableItems {
    val id: Int
    val image: Int
}