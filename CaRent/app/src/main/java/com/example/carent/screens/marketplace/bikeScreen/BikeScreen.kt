package com.example.carent.screens.marketplace.bikeScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.carent.screens.util.components.CustomTopAppBar
import com.example.carent.screens.util.grid.VerticalGrid
import com.example.carent.ui.theme.CaRentTheme

@Composable
fun BikeScreen(
    navController: NavController,
    vm: BikeScreenViewModel = viewModel()
) {
    val state = vm.state.value
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF2B2E31))
    ) {
        Column {
            CustomTopAppBar(navController, "", true )
            VerticalGrid(state.items)
            println(state.items)
        }
    }
}


@Preview(showBackground = true)
@Composable
fun BikeViewPreview(vm: BikeScreenViewModel = viewModel()) {
    val state = vm.state.value
    CaRentTheme {
        VerticalGrid(state.items)
    }
}