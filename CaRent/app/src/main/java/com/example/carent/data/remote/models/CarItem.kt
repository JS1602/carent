package com.example.carent.data.remote.models

import com.google.gson.annotations.SerializedName

data class CarItem(
    @SerializedName("city_mpg")
    val cityMpg: Int,
    @SerializedName("class")
    val classX: String,
    @SerializedName("combination_mpg")
    val combinationMpg: Int,
    @SerializedName("cylinders")
    val cylinders: Int,
    @SerializedName("displacement")
    val displacement: Double,
    @SerializedName("drive")
    val drive: String,
    @SerializedName("fuel_type")
    val fuelType: String,
    @SerializedName("highway_mpg")
    val highwayMpg: Int,
    @SerializedName("make")
    val make: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("transmission")
    val transmission: String,
    @SerializedName("year")
    val year: Int
)
