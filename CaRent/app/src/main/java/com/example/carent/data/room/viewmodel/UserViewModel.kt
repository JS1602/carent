package com.example.carent.data.room.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


class UserViewModel(private val userRepository: UserRepository) : ViewModel() {

    private val _registrationSuccess = MutableStateFlow<Boolean?>(null)
    val registrationSuccess: StateFlow<Boolean?> = _registrationSuccess

    private val _loginSuccess = MutableStateFlow<Boolean?>(null)
    val loginSuccess: StateFlow<Boolean?> = _loginSuccess

    fun registerUser(user: User) {
        viewModelScope.launch {
            try {
                userRepository.addUser(user)
                _registrationSuccess.value = true
            } catch (e: Exception) {
                _registrationSuccess.value = false
            }
        }
    }

    fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            val user = userRepository.getUserByEmailAndPassword(email, password)
            _loginSuccess.value = user != null
        }
    }
}





//class UserViewModel(application: Application): AndroidViewModel(application) {
//
//    private val repository: UserRepository
//    private val readAllData: LiveData<List<User>>
//
//
//    init{
//        val userDao = UserDatabase.getDatabase(application).userDao()
//        repository = UserRepository(userDao)
//        readAllData = repository.readAllData
//    }
//
//    fun addUser(user: User) {
//      viewModelScope.launch(Dispatchers.IO){
//          repository.addUser(user)
//      }
//    }
//
//}
