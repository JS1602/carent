package com.example.carent.screens.util.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.Notifications
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.carent.R
import com.example.carent.screens.navigation.Routes

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomTopAppBar(navController: NavController, title: String, showBackIcon: Boolean) {

    val currentRoute = navController.currentDestination?.route

    CenterAlignedTopAppBar(
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(containerColor = Color.Transparent),
        title = {
            if(title == "") {
                Image(
                    modifier = Modifier
                        .height(58.dp),
                    contentScale = ContentScale.Fit,
                    painter = painterResource(id = R.drawable.logo_without_text_white),
                    contentDescription = ""
                )
            } else {
                Text(
                    text = title,
                    color = Color.White,
                    textAlign = TextAlign.Start
                )
            }

        },
        navigationIcon = {
            if (showBackIcon && navController.previousBackStackEntry != null) {
                IconButton(
                    onClick = {
                        navController.navigateUp()
                    }
                ) {
                    Icon(
                        modifier = Modifier.scale(1.5f),
                        imageVector = Icons.Filled.ArrowBack,
                        tint = Color.White,
                        contentDescription = "Back"
                    )
                }
            }
        },
        actions = {
            if (currentRoute in listOf(
                    Routes.Cars.route,
                    Routes.Bikes.route,
                    Routes.Boats.route,
                    Routes.Planes.route
                )
            ){
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(
                        imageVector = Icons.Outlined.Notifications,
                        contentDescription = "Notifications Bell",
                        modifier = Modifier
                            .size(30.dp),
                        tint = Color(0xFFF6B266)
                    )
                }
            }

        }
    )
}