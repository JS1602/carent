package com.example.carent.data.room.repository

import com.example.carent.data.room.model.Favorite
import com.example.carent.data.room.model.FavoriteDao
import com.example.carent.data.room.model.UserWithFavorites
import kotlinx.coroutines.flow.Flow

class FavoriteRepository(private val dao: FavoriteDao) {


    fun getAllFavorites(): Flow<List<Favorite>>{
        return dao.getAllFavorites()
    }


    suspend fun getFavoritesByUserId(userId: Int): Favorite {
        return dao.getFavoritesByUserId(userId)
    }


    fun getUserWithFavorites(userId: Int): UserWithFavorites {
        return dao.getUserWithFavorites(userId)
    }


    suspend fun insertFavorite(favorite: Favorite) {
        dao.insertFavorite(favorite)
    }


    suspend fun updateFavorite(favorite: Favorite) {
        dao.updateFavorite(favorite)
    }


    suspend fun deleteFavorite(favorite: Favorite){
        dao.deleteFavorite(favorite)
    }


}