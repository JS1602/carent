package com.example.carent.data.remote.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carent.data.remote.models.CarItem
import com.example.carent.data.remote.service.CarApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarRepository(private val apiService: CarApiService) {
    fun fetchCars(limit: Int, make: String): LiveData<List<CarItem>> {
        val data = MutableLiveData<List<CarItem>>()
        apiService.getCars(limit, make).enqueue(object : Callback<List<CarItem>> {
            override fun onResponse(
                call: Call<List<CarItem>>,
                response: Response<List<CarItem>>
            ) {
                if (response.isSuccessful) {
                    data.value = response.body()
                    Log.d("CarRepository_Info", "onResponse: Success")
                    Log.d("CarRepository_Info", "onResponse: $call")
                    Log.d("CarRepository_Info", "onResponse: ${response.body()}")
                } else {
                    Log.d("CarRepository_Info", "onResponse: Error ${response.code()}")
                }
            }
            override fun onFailure(call: Call<List<CarItem>>, t: Throwable) {
                return t.printStackTrace()
            }
        })
        return data
    }
}