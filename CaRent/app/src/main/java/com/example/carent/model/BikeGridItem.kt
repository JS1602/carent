package com.example.carent.model

import com.example.carent.R

data class BikeGridItem(
    override val id: Int,
    val manufacturer: String,
    val model: String,
    val dailyCost: Int,
    val distanceToUser: Int,
    override val image: Int = R.drawable.colored_waves_removebg,
) : DisplayableItems