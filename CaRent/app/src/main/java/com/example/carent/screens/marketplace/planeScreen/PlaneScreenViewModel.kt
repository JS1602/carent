package com.example.carent.screens.marketplace.planeScreen

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.carent.data.remote.api.PlaneApiImplementation
import com.example.carent.data.remote.repository.PlaneRepository
import com.example.carent.model.PlaneGridItem
import kotlinx.coroutines.launch
import java.text.DecimalFormat

class PlaneScreenViewModel: ViewModel() {

    private val repository = PlaneRepository(PlaneApiImplementation.createApiService())
    private val _state = mutableStateOf<List<PlaneGridItem>>(emptyList())
    val state: MutableState<List<PlaneGridItem>> = _state

    init {
        fetchPlanes()
    }

    private fun fetchPlanes() {
        viewModelScope.launch {
            val fetchedPlanes = repository.fetchPlanes(30, "Gulfstream")
            var i = 0;
            fetchedPlanes.observeForever { planesList ->
                _state.value = planesList.map { planeItem ->
                    PlaneGridItem(
                        id = i++,
                        manufacturer = minimizeStringLength(planeItem.manufacturer),
                        model = planeItem.model,
                        dailyCost = randomCost(),
                        distanceToUser = randomDistance(),
                    )
                } ?: emptyList()
            }
        }
    }

    private fun randomCost(): Double {
        val cost = 525.00 + (Math.random() * (1500.00 - 525.00))
        val decimalFormat = DecimalFormat("####.##")
        return decimalFormat.format(cost).toDouble()
    }

    private fun randomDistance(): Int {
        return (32 + (Math.random() * (321 - 32))).toInt()
    }

    private fun minimizeStringLength(string: String): String {
        return string.substringBefore(" ")
    }

}