package com.example.carent.data.remote.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.carent.data.remote.models.PlaneItem
import com.example.carent.data.remote.service.PlaneApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlaneRepository(private val apiService: PlaneApiService) {

    fun fetchPlanes(limit: Int, manufacturer: String): LiveData<List<PlaneItem>> {
        val data = MutableLiveData<List<PlaneItem>>()
        apiService.getPlanes(limit, manufacturer).enqueue(object : Callback<List<PlaneItem>> {
            override fun onResponse(
                call: Call<List<PlaneItem>>,
                response: Response<List<PlaneItem>>
            ) {
                if (response.isSuccessful) {
                    data.value = response.body()
                    Log.d("PlaneRepository_Info", "onResponse: Success")
                    Log.d("PlaneRepository_Info", "onResponse: $call")
                    Log.d("PlaneRepository_Info", "onResponse: ${response.body()}")
                } else {
                    Log.d("PlaneRepository_Info", "onResponse: Error ${response.code()}")
                }
            }
            override fun onFailure(call: Call<List<PlaneItem>>, t: Throwable) {
                return t.printStackTrace()
            }
        })
        return data
    }
}