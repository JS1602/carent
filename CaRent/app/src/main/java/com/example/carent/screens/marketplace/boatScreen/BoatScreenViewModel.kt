package com.example.carent.screens.marketplace.boatScreen

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import com.example.carent.R
import com.example.carent.model.GridItem

class BoatScreenViewModel: ViewModel() {
    private val _state = mutableStateOf(BoatScreenState())
    val state: MutableState<BoatScreenState> = _state
    init {
        _state.value = state.value.copy(
            items = listOf(
                GridItem(1, "Car", "CarName", 240, R.drawable.logo, Color.Blue),
                GridItem(2, "Car", "CarName", 240, R.drawable.logo,Color.White),
                GridItem(3, "Car", "CarName", 240, R.drawable.logo,Color.Yellow),
                GridItem(4, "Car", "CarName", 240, R.drawable.logo,Color.Black),
                GridItem(5, "Car", "CarName", 240, R.drawable.logo,Color.Red),
                GridItem(6, "Car", "CarName", 240, R.drawable.logo,Color.Magenta),
                GridItem(7, "Car", "CarName", 240, R.drawable.logo,Color.Green),
                GridItem(8, "Car", "CarName", 240, R.drawable.logo,Color.DarkGray),
                GridItem(9, "Car", "CarName", 240, R.drawable.logo,Color.Gray),
            )
        )
    }
}