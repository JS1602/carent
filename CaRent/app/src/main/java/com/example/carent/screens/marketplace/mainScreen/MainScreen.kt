@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.carent.screens.marketplace.mainScreen

import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.History
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Notifications
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.carent.R
import com.example.carent.model.BottomNavItem
import com.example.carent.model.CardNavigationItem
import com.example.carent.screens.navigation.Routes
import com.example.carent.screens.util.background.LoginBackground

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MainScreen(
    navController: NavHostController,
    transferredId: Int? = null,
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = { ScaffoldBody(navController) },
        bottomBar = { CustomMainBottomBar(navController) },
        topBar = { TopAppProfileBar(navController, transferredId) },
    )
}

@Preview
@Composable
fun TestScreenView() {
    MainScreen(rememberNavController())
}

// BODY FOR THE SCAFFOLD HERE
@Composable
fun ScaffoldBody(navController: NavController) {
    LoginBackground()
    Column(
        modifier = Modifier.padding(16.dp, 190.dp, 16.dp, 16.dp)
    ) {
        Slider()
        ServiceButtons(navController)
        RecommendationText()
        OfferCard()
    }
}


// ---------------------------------------- TopAppBar ----------------------------------------

@Composable
fun TopAppProfileBar(navController: NavController, transferredId: Int? = null) {
    val vm: MainScreenViewModel = viewModel()
    vm.getUserDetails(transferredId)

    Column(
        modifier = Modifier
            .clip(RoundedCornerShape(bottomStart = 14.dp, bottomEnd = 14.dp))
            .fillMaxWidth()
            .background(Color(0xFF1D1E1F))
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .padding(start = 14.dp)
                    .height(75.dp)
            ) {
                Image(
                    painter = painterResource(R.drawable.profile_placeholder),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .clickable(onClick = { navController.navigate(Routes.UserProfile.route) })
                        .clip(CircleShape)
                        .size(55.dp)
                )
            }
            Column(
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .height(75.dp)
                    .width(275.dp)
            ) {
                Text(
                    text = "Welcome,",
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.offset(x = 10.dp)
                )
                Text(
                    text = (vm.userDetails.value?.firstName + " " + vm.userDetails.value?.lastName) + "!",
                    color = Color.White,
                    fontWeight = FontWeight.Normal,
                    modifier = Modifier.offset(x = 10.dp)
                )
            }
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.height(75.dp)
            ) {
                Icon(
                    imageVector = Icons.Outlined.Notifications,
                    contentDescription = "Notifications Bell",
                    modifier = Modifier
                        .clickable(onClick = { navController.navigate(Routes.Main.route) })
                        .size(30.dp),
                    tint = Color(0xFFF6B266)
                )
            }

        }
        Row(
            modifier = Modifier.padding(bottom = 24.dp)
        ) {
            SearchBarMain()
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchBarMain() {
    var text by rememberSaveable { mutableStateOf("") }
    var active by rememberSaveable { mutableStateOf(false) }
    val items = remember {
        mutableListOf(
            "Cars",
            "Bikes",
            "Boats",
            "Planes"
        )
    }

    SearchBar(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp),
        shape = RoundedCornerShape(8.dp),
        query = text,
        onQueryChange = { text = it },
        onSearch = {
            items.add(text)
            active = false
            text = ""
        },
        active = active,
        onActiveChange = { active = it },
        placeholder = { Text("Search...", color = Color.Black) },
        leadingIcon = { Icon(Icons.Default.Search, contentDescription = "Search Icon") },
        trailingIcon = {
            if (active) {
                Icon(
                    Icons.Default.Close,
                    modifier = Modifier.clickable(onClick = {
                        if (text.isNotEmpty())
                            text = ""
                        else
                            active = false
                    }),
                    contentDescription = "Close Icon"
                )
            }
        }
    ) {
        items.forEach {
            Row(
                modifier = Modifier.padding(all = 14.dp),
            ) {
                Icon(
                    Icons.Default.History,
                    modifier = Modifier.padding(end = 10.dp),
                    contentDescription = "History Icon"
                )
                Text(text = it)
            }
        }
    }
}

// ---------------------------------------- BottomNav ----------------------------------------

@Composable
fun CustomMainBottomBar(navController: NavController) {
    val bottomNavItems = listOf(
        BottomNavItem(
            name = "Home",
            route = Routes.Main.route,
            icon = Icons.Outlined.Home
        ),
        BottomNavItem(
            name = "Favorites",
            route = Routes.Main.route,
            icon = Icons.Outlined.Favorite
        ),
        BottomNavItem(
            name = "Settings",
            route = Routes.UserProfile.route,
            icon = Icons.Outlined.Settings
        )
    )

    NavigationBar(
        containerColor = Color.Transparent,
        modifier = Modifier
            .height(55.dp)
            .background(Color(0xFF1D1E1F))
    ) {
        bottomNavItems.forEach { item ->
            NavigationBarItem(
                selected = false,
                onClick = { navController.navigate(item.route) },
                label = {
                    Text(
                        modifier = Modifier.offset(0.dp, 10.dp),
                        text = item.name,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White
                    )
                },
                icon = {
                    Icon(
                        modifier = Modifier.offset(0.dp, (-10).dp),
                        imageVector = item.icon,
                        contentDescription = "${item.name} Icon",
                        tint = Color.White
                    )
                }
            )
        }
    }
}

// ---------------------------------------- Slider ----------------------------------------
@Preview
@Composable
fun SliderPrev() {
    Slider()
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Slider() {

    //Api call for some random images could be used here. AsyncImage somehow does not like my api calls...
    val images = listOf(
        "https://i.ibb.co/tHjbQJy/background-gca6af8c85-640.png",
        "https://i.ibb.co/KVBBkQS/background-g8a8531178-640.png",
        "https://i.ibb.co/bJvJCyg/background-g593069cf3-640.png",
        "https://i.ibb.co/G3952nk/triangles-ge8232e699-640.png",
        "https://i.ibb.co/cXH81Np/colorful-g58e99ec37-640.png",
    )

    Card(
        shape = RoundedCornerShape(16.dp),
    ) {
        val pagerState = rememberPagerState()
        AutoSlidingCarousel(
            pagerState = pagerState,
            itemsCount = images.size,
            itemContent = { index ->
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(images[index])
                        .build(),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .height(200.dp)
                        .fillMaxWidth()
                )
            }
        )
    }
}

@Composable
fun DotsIndicator(
    modifier: Modifier = Modifier,
    totalDots: Int,
    selectedIndex: Int,
    selectedColor: Color = Color.Yellow,
    unSelectedColor: Color = Color.Gray,
    dotSize: Dp
) {
    LazyRow(
        modifier = modifier
            .wrapContentWidth()
            .wrapContentHeight()
    ) {
        items(totalDots) { index ->
            IndicatorDot(
                color = if (index == selectedIndex) selectedColor else unSelectedColor,
                size = dotSize
            )

            if (index != totalDots - 1) {
                Spacer(modifier = Modifier.padding(horizontal = 2.dp))
            }
        }
    }
}

@Composable
fun IndicatorDot(
    modifier: Modifier = Modifier,
    size: Dp,
    color: Color
) {
    Box(
        modifier = modifier
            .size(size)
            .clip(CircleShape)
            .background(color)
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun AutoSlidingCarousel(
    modifier: Modifier = Modifier,
//    autoSlideDuration: Long = 3000,
    pagerState: PagerState,
    itemsCount: Int,
    itemContent: @Composable (index: Int) -> Unit,
) {
//    val isDragged by pagerState.interactionSource.collectIsDraggedAsState()
//    val key by remember { mutableStateOf(0) }
//    LaunchedEffect(key,pagerState.currentPage) {
//        launch {
//            delay(autoSlideDuration)
//            pagerState.animateScrollToPage((pagerState.currentPage + 1) % itemsCount)
//        }
//    }

    Box(
        modifier = modifier.fillMaxWidth(),
    ) {
        HorizontalPager(pageCount = itemsCount, state = pagerState, modifier = modifier.fillMaxWidth()) { page ->
            itemContent(page)
        }
        Surface(
            modifier = Modifier
                .padding(bottom = 8.dp)
                .align(Alignment.BottomCenter),
            shape = CircleShape,
            color = Color.Black.copy(alpha = 0.5f)
        ) {
            DotsIndicator(
                modifier = Modifier.padding(horizontal = 8.dp, vertical = 6.dp),
                totalDots = itemsCount,
//                selectedIndex = if (isDragged) pagerState.currentPage else pagerState.targetPage,
                selectedIndex = pagerState.currentPage,
                dotSize = 8.dp
            )
        }
    }
}


// ---------------------------------------- Buttons ----------------------------------------
@Preview
@Composable
fun ServiceButtonPrev() {
    ServiceButtons(rememberNavController())
}

@Composable
fun ServiceButtons(navController: NavController) {
    val cardNavItems = listOf(
        CardNavigationItem(
            title = "Cars",
            route = Routes.Cars.route,
            icon = R.drawable.sharp_directions_car_24
        ),
        CardNavigationItem(
            title = "Motorbikes",
            route = Routes.Bikes.route,
            icon = R.drawable.sharp_two_wheeler_24
        ),
        CardNavigationItem(
            title = "Boats",
            route = Routes.Boats.route,
            icon = R.drawable.sharp_directions_boat_filled_24
        ),
        CardNavigationItem(
            title = "Planes",
            route = Routes.Planes.route,
            icon = R.drawable.sharp_airplanemode_active_24
        ),
    )
    Column {
        Text(
            modifier = Modifier.padding(8.dp, 16.dp, 8.dp, 8.dp),
            text = "Services",
            color = Color.White,
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = FontWeight.Bold
        )
        Row {
            cardNavItems.forEach { item ->
                ServiceCards(
                    navController,
                    title = item.title,
                    route = item.route,
                    icon = item.icon
                )
            }
        }
    }
}

@Composable
fun ServiceCards(navController: NavController, title: String, route: String, icon: Int) {
    Card(
        shape = RoundedCornerShape(12.dp),
        modifier = Modifier
            .clickable { navController.navigate(route) }
            .padding(all = 8.dp)
            .height(75.dp)
            .width(75.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 10.dp,
        ),
        content = {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxSize()
            ) {
                Icon(
                    painter = painterResource(icon),
                    contentDescription = "",
                    tint = Color.Black,
                    modifier = Modifier.size(45.dp)
                )
                Text(
                    text = title,
                    color = Color.Black,
                    style = MaterialTheme.typography.labelMedium,
                    textAlign = TextAlign.Center
                )
            }
        }
    )
}

// ---------------------------------------- Recommendation ----------------------------------------


@Preview
@Composable
fun RecommendationTextPreview() {
    RecommendationText()
}


@Composable
fun RecommendationText() {
    Column {
        Text(
            modifier = Modifier
                .padding(8.dp, 16.dp, 8.dp, 8.dp),
            text = "Recommendations",
            color = Color.White,
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = FontWeight.Bold
        )
        Column(
            modifier = Modifier.padding(start = 14.dp, end = 14.dp)
        ) {
            Text(
                text = "Lore ipsum dolor sit amet, consectetur adipiscing elit, " +
                        "sed do eiusmod tempor incididunt ut labore et dolore magna " +
                        "aliqua. Ut enim ad minim veniam, quis nostrud",
                color = Color.White,
                style = MaterialTheme.typography.bodyMedium,
                textAlign = TextAlign.Center
            )
        }
    }
}

// ---------------------------------------- OfferScreen ----------------------------------------
@Preview
@Composable
fun OfferPreview() {
    OfferCard()
}

@Composable
fun OfferCard() {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier.padding(top = 30.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(0xFF1D1E1F))
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .padding(start = 14.dp)
                    .height(75.dp)
            ) {
                Image(
                    painter = painterResource(R.drawable.placeholder),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .clip(RoundedCornerShape(16.dp))
                        .size(100.dp)
                )
            }
            Column(
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .height(100.dp)
                    .width(275.dp)
            ) {
                Text(
                    text = "Max Mustermann",
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.offset(x = 10.dp)
                )
                Text(
                    text = "Most sought after CaRenter",
                    color = Color.White,
                    fontWeight = FontWeight.Normal,
                    modifier = Modifier
                        .offset(x = 10.dp)
                        .padding(top = 4.dp, bottom = 4.dp)
                )
                Row {
                    Icon(
                        imageVector = Icons.Filled.Star,
                        contentDescription = "",
                        tint = Color(0xFFF6B266),
                        modifier = Modifier
                            .padding(end = 10.dp)
                            .size(20.dp)
                            .offset(x = 15.dp)
                    )
                    Text(
                        text = "4.5",
                        color = Color.White,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier
                            .offset(x = 10.dp)
                            .padding(end = 5.dp)
                    )
                    Text(
                        text = "(37 Reviews)",
                        color = Color.White,
                        fontWeight = FontWeight.Normal,
                        modifier = Modifier.offset(x = 10.dp)
                    )
                }
            }
        }
    }
}


