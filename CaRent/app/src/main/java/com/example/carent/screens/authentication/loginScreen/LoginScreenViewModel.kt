package com.example.carent.screens.authentication.loginScreen

import android.app.Application
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import com.example.carent.data.room.database.UserDatabase
import com.example.carent.data.room.model.User
import com.example.carent.data.room.repository.UserRepository
import kotlinx.coroutines.flow.Flow

class LoginScreenViewModel(application: Application): AndroidViewModel(application) {

    private val readAllData: Flow<List<User>>
    private val repository: UserRepository

    private val _passwordValue = mutableStateOf("")
    private val passwordValue: State<String> = _passwordValue

    private val _emailValue = mutableStateOf("")
    private val emailValue: State<String> = _emailValue

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
        readAllData = repository.getAllUsers()
        Log.d("####READ DATA######", "init: $readAllData")
    }

    fun setEmail(email: String) {
        _emailValue.value = email
    }

    fun setPassword(password: String) {
        _passwordValue.value = password
    }

    fun clearInputFields() {
        _emailValue.value = ""
        _passwordValue.value = ""
    }

    suspend fun getUserId(): Int? {
        val email = emailValue.value
        val password = passwordValue.value
        return repository.getUserId(email, password)
    }

    suspend fun loginUser(): Boolean {
            val email = emailValue.value
            val password = passwordValue.value
            Log.d("##########", "loginUser: $email, $password")
            val user = repository.getUserByEmailAndPassword(email, password)
            return user != null
    }
}


































//
//@HiltViewModel
//class LoginViewModel @Inject constructor(var usersUseCase: UsersUseCase): ViewModel(){
//
//    private val _GetUserLoginDataStatus = MutableLiveData<Resource<Users>>()
//
//    val getUserLoginDataStatus: MutableLiveData<Resource<Users>> = _GetUserLoginDataStatus
//
//    private val _GetUserProfileDataStatus = MutableLiveData<Resource<Users>>()
//
//    val getUserProfileDataStatus: MutableLiveData<Resource<Users>> = _GetUserProfileDataStatus
//
//
//    fun getUserLoginDataStatus(mobNum:String,password:String) {
//        viewModelScope.launch {
//            _GetUserLoginDataStatus.postValue(Resource.loading(null))
//            try {
//                val data = usersUseCase.getUserLoginVerify(mobNum,password)
//                _GetUserLoginDataStatus.postValue(Resource.success(data, 0))
//            } catch (exception: Exception) {
//                _GetUserLoginDataStatus.postValue(Resource.error(null, exception.message!!))
//            }
//        }
//    }
//
//    fun getUserProfileData(id:Long){
//        viewModelScope.launch {
//            _GetUserProfileDataStatus.postValue(Resource.loading(null))
//            try {
//                val data = usersUseCase.getUserData(id)
//                _GetUserProfileDataStatus.postValue(Resource.success(data, 0))
//            } catch (exception: Exception) {
//                _GetUserProfileDataStatus.postValue(Resource.error(null, exception.message!!))
//            }
//        }
//    }
//
//
//}




//class LoginViewModel(private val userRepository: UserRepository): ViewModel(){
//
//    private val _userInserted = MutableLiveData<Boolean>()
//    val userInserted: LiveData<Boolean> get() = _userInserted
//
//    private val _user = MutableLiveData<User?>()
//    val user: LiveData<User?> get() = _user
//
//
//    fun insertUser(user: User) {
//        viewModelScope.launch{
//            userRepository.insertUser(user)
//            _userInserted.value = true
//        }
//    }
//
//    fun getUserByEmail(email: String) {
//        viewModelScope.launch {
//            _user.value = userRepository.getUserByEmail(email)
//        }
//    }
//}