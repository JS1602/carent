package com.example.carent.screens.marketplace.carScreen


import com.example.carent.model.GridItem

data class CarScreenState(
    val items: List<GridItem> = mutableListOf()
)
