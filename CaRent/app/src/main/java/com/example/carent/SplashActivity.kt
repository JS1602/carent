package com.example.carent

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    private val splashDuration = 2800L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        // Delay for the splash duration and transition to the main screen
        Handler(Looper.getMainLooper()).postDelayed({
            navigateToMainActivity(this)
            finish()
        }, splashDuration)

    }
}

private fun navigateToMainActivity(context: Context) {
    val intent = Intent(context, MainActivity::class.java)
    context.startActivity(intent)
    if (context is Activity) {
        context.finish()
    } // Optional: Finish the splash screen activity so it's not accessible via back button
}