package com.example.carent.screens.navigation.nav_graph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import com.example.carent.screens.authentication.forgotPasswordScreen.ForgotPasswordScreen
import com.example.carent.screens.authentication.loginScreen.LoginScreen
import com.example.carent.screens.authentication.signUpScreen.SignUpScreen
import com.example.carent.screens.navigation.AUTH_GRAPH_ROUTE
import com.example.carent.screens.navigation.Routes

fun NavGraphBuilder.authNavGraph(
    navController: NavHostController
){
    // ------------------ AUTHENTICATION ------------------
    navigation(
        startDestination = Routes.Login.route,
        route = AUTH_GRAPH_ROUTE
    ){

        // ------- Login -------
        composable(
            route = Routes.Login.route
        ){
            LoginScreen(navController = navController)
        }
        // ------- SignUp -------
        composable(
            route = Routes.SignUp.route
        ){
            SignUpScreen(navController = navController)
        }
        // ------- ForgotPassword -------
        composable(
            route = Routes.ForgotPassword.route
        ){
            ForgotPasswordScreen(navController = navController)
        }
    }
}