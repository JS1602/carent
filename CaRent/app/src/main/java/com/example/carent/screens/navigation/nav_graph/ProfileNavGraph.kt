package com.example.carent.screens.navigation.nav_graph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import com.example.carent.screens.navigation.PROFILE_GRAPH_ROUTE
import com.example.carent.screens.navigation.Routes
import com.example.carent.screens.userprofile.profileScreen.ProfileScreen


fun NavGraphBuilder.profileNavGraph(
    navController: NavHostController
) {
    navigation(
        startDestination = Routes.UserProfile.route,
        route = PROFILE_GRAPH_ROUTE
    ) {
        composable(
            route = Routes.UserProfile.route
        ) {
            ProfileScreen(navController = navController)
        }
    }
}